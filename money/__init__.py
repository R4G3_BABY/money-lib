__name__ = 'money'
__version__ = '2.1.0'

from .currency import Currency
from .exchange import xrates
from .money import Money
